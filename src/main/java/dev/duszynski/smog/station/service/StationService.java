package dev.duszynski.smog.station.service;

import dev.duszynski.smog.common.exception.ResourceNotFoundException;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Station;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StationService {

    private static final int FIRST_PAGE_INDEX = 0;

    private final StationRepository stationRepository;

    public StationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    public Station getByIdOrThrow(long idStation) {
        return stationRepository.findById(idStation).orElseThrow(ResourceNotFoundException::new);
    }

    public Page<Station> fetchStationsWithCriteria(final Integer pageSize, final Integer pageNumber) {
        return stationRepository.findAll(buildAllStationsPageRequest(pageSize, pageNumber));
    }

    private PageRequest buildAllStationsPageRequest(final Integer pageSize, final Integer pageNumber) {
        return PageRequest.of(
                pageNumber == null ? FIRST_PAGE_INDEX : pageNumber,
                pageSize == null ? Integer.MAX_VALUE : pageSize,
                Sort.Direction.ASC,
                Station.NAME);
    }
}
