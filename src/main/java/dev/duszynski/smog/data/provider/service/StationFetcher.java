package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.data.provider.model.dto.GiosStationDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Component
class StationFetcher {

    private static final String STATIONS_URL = "http://api.gios.gov.pl/pjp-api/rest/station/findAll";

    private final RestTemplate restTemplate;

    public StationFetcher() {
        this.restTemplate = new RestTemplate();
    }

    List<GiosStationDTO> fetchAll() {
        ResponseEntity<List<GiosStationDTO>> response = restTemplate.exchange(STATIONS_URL, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });

        if (response.getStatusCode() != HttpStatus.OK) {
            return Collections.emptyList();
        }

        return response.getBody();
    }
}
