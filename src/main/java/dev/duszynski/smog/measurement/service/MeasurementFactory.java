package dev.duszynski.smog.measurement.service;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import dev.duszynski.smog.measurement.model.entity.Pm10Measurement;
import dev.duszynski.smog.measurement.model.entity.Pm25Measurement;

public class MeasurementFactory {

    public static <E extends Measurement> E createFor(final MeasurementType type) {
        switch (type) {
            case PM10:
                return (E) new Pm10Measurement();
            case PM25:
                return (E) new Pm25Measurement();
            default:
                throw new IllegalArgumentException();
        }
    }
}
