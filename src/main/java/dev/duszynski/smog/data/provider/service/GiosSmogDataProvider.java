package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.data.provider.model.bo.MeasurementBO;
import dev.duszynski.smog.data.provider.model.bo.SensorBO;
import dev.duszynski.smog.data.provider.model.bo.StationBO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class GiosSmogDataProvider {

    private final StationFetchService stationFetchService;
    private final MeasurementFetchService measurementFetchService;
    private final SensorFetchService sensorFetchService;

    public GiosSmogDataProvider(final StationFetchService stationFetchService,
                                final MeasurementFetchService measurementFetchService,
                                final SensorFetchService sensorFetchService) {
        this.stationFetchService = stationFetchService;
        this.measurementFetchService = measurementFetchService;
        this.sensorFetchService = sensorFetchService;
    }

    public List<StationBO> fetchAllStations() {
        return stationFetchService.fetchAllAvailableStations();
    }

    public List<SensorBO> fetchSensorsForStation(final long idStation) {
        return sensorFetchService.fetchSensorsForStation(idStation);
    }

    public Optional<MeasurementBO> fetchMeasurementForSensor(final long idSensor) {
        return measurementFetchService.fetchLastMeasurementForSensor(idSensor);
    }
}
