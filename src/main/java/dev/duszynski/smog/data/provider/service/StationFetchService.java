package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.data.provider.model.bo.StationBO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
class StationFetchService {

    private final StationFetcher stationFetcher;

    public StationFetchService(StationFetcher stationFetcher) {
        this.stationFetcher = stationFetcher;
    }

    public List<StationBO> fetchAllAvailableStations() {
        return stationFetcher.fetchAll().stream().map(StationBO::of).collect(Collectors.toList());
    }

}
