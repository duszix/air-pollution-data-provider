package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.data.provider.exception.NotKnownMeasurementTypeCodeException;
import dev.duszynski.smog.data.provider.model.bo.MeasurementBO;
import dev.duszynski.smog.data.provider.model.dto.GiosMeasurementDTO;
import dev.duszynski.smog.data.provider.model.dto.GiosMeasurementValueDTO;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Optional;

@Component
class MeasurementFetchService {

    private static final DateTimeFormatter MEASUREMENT_DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("CET"));

    private final MeasurementFetcher measurementFetcher;

    public MeasurementFetchService(MeasurementFetcher measurementFetcher) {
        this.measurementFetcher = measurementFetcher;
    }

    public Optional<MeasurementBO> fetchLastMeasurementForSensor(final long idSensor) {
        Optional<GiosMeasurementDTO> response = measurementFetcher.fetchCurrentMeasurementsForSensor(idSensor);
        if (response.isEmpty()) {
            return Optional.empty();
        }

        final GiosMeasurementDTO giosMeasurementDTO = response.get();
        final Optional<GiosMeasurementValueDTO> lastMeasurementValue = fetchLastWithValue(giosMeasurementDTO);
        if (lastMeasurementValue.isEmpty()) {
            return Optional.empty();
        }

        final GiosMeasurementValueDTO value = lastMeasurementValue.get();
        return Optional.of(
                MeasurementBO.builder()
                        .type(getMeasurementTypeOrThrow(giosMeasurementDTO))
                        .value(value.getValue())
                        .measuredAt(parseTimestamp(value.getDate()))
                        .build());
    }

    /**
     * API returns measurements with value 0 which means measurement was not taken yet.
     *
     * @return last measurement with value greater than 0
     */
    Optional<GiosMeasurementValueDTO> fetchLastWithValue(final GiosMeasurementDTO dto) {
        return dto.getValues()
                .stream()
                .filter(v -> v.getValue() != 0)
                .max(Comparator.comparing(v -> parseTimestamp(v.getDate())));
    }

    private MeasurementType getMeasurementTypeOrThrow(final GiosMeasurementDTO giosMeasurementDTO) {
        final String code = giosMeasurementDTO.getKey();
        return MeasurementType.getByCode(code).orElseThrow(() -> new NotKnownMeasurementTypeCodeException(code));
    }

    private Instant parseTimestamp(final String rawTimestamp) {
        return Instant.from(MEASUREMENT_DATE_TIME_FORMATTER.parse(rawTimestamp));
    }

}
