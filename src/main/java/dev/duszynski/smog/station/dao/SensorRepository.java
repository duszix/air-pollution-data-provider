package dev.duszynski.smog.station.dao;

import dev.duszynski.smog.station.model.entity.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface SensorRepository extends JpaRepository<Sensor, Long> {

    Collection<Sensor> findAllByStationId(final long idStation);

}
