package dev.duszynski.smog.data.provider.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiosMeasurementValueDTO {

    private String date;
    private double value;
}
