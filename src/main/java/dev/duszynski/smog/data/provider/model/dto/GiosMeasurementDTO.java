package dev.duszynski.smog.data.provider.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GiosMeasurementDTO {

    private String key;
    private List<GiosMeasurementValueDTO> values = new ArrayList<>();
}
