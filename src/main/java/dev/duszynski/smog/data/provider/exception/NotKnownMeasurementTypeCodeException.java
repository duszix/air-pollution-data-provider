package dev.duszynski.smog.data.provider.exception;

public class NotKnownMeasurementTypeCodeException extends RuntimeException {

    public NotKnownMeasurementTypeCodeException(final String code) {
        super(String.format("Not known measurement type: %s", code));
    }
}
