package dev.duszynski.smog.measurement.job;

import com.google.common.collect.Sets;
import dev.duszynski.smog.common.model.type.EntityStatus;
import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.service.MeasurementService;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Sensor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class FetchMeasurementJob {

    private static final Set<MeasurementType> HANDLED_MEASUREMENT_TYPES =
            Sets.newHashSet(MeasurementType.PM10, MeasurementType.PM25);

    private final StationRepository stationRepository;
    private final MeasurementService measurementService;

    public FetchMeasurementJob(StationRepository stationRepository, MeasurementService measurementService) {
        this.stationRepository = stationRepository;
        this.measurementService = measurementService;
    }

    @Transactional
    @Scheduled(cron = "0 0/3 * * * *")
    public void fetchLatestMeasurements() {
        getAllSensorToFetchMeasurementsFor().forEach(measurementService::fetchNewestMeasurementForSensor);
    }

    private Set<Sensor> getAllSensorToFetchMeasurementsFor() {
        return stationRepository.findAllByStatus(EntityStatus.ACTIVE)
                .stream()
                .flatMap(station -> station.getSensors().stream())
                .filter(sensor -> HANDLED_MEASUREMENT_TYPES.contains(sensor.getMeasuredParameterCode()))
                .collect(Collectors.toSet());
    }

}
