package dev.duszynski.smog.station.service;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.config.AsyncConfiguration;
import dev.duszynski.smog.data.provider.exception.NotKnownMeasurementTypeCodeException;
import dev.duszynski.smog.data.provider.model.bo.SensorBO;
import dev.duszynski.smog.data.provider.service.GiosSmogDataProvider;
import dev.duszynski.smog.station.dao.SensorRepository;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Sensor;
import dev.duszynski.smog.station.model.entity.Station;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SensorService {

    private final GiosSmogDataProvider giosSmogDataProvider;
    private final StationRepository stationRepository;
    private final SensorRepository sensorRepository;

    public SensorService(final GiosSmogDataProvider giosSmogDataProvider,
                         final StationRepository stationRepository,
                         final SensorRepository sensorRepository) {
        this.giosSmogDataProvider = giosSmogDataProvider;
        this.stationRepository = stationRepository;
        this.sensorRepository = sensorRepository;
    }

    @Async(AsyncConfiguration.SMOG_POOL_EXECUTOR)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void fetchAndAddNewSensorsForStation(final long idStation) {
        doFetchAndAddNewSensorsForStation(idStation);
    }

    void doFetchAndAddNewSensorsForStation(long idStation) {
        final Station station = stationRepository.findById(idStation)
                .orElseThrow(() -> new RuntimeException("Could not find station"));

        final List<Sensor> newSensors = fetchNewSensors(station);

        sensorRepository.saveAll(newSensors);
        station.getSensors().addAll(newSensors);

        log.info("Fetched {} new sensors for station {}", newSensors.size(), station.getName());
    }

    private List<Sensor> fetchNewSensors(final Station station) {
        final Set<Long> sensorExternalIds = station.getSensors().stream()
                .map(Sensor::getExternalApiId)
                .collect(Collectors.toSet());

        return giosSmogDataProvider.fetchSensorsForStation(station.getExternalId())
                .stream()
                .filter(s -> !sensorExternalIds.contains(s.getId()))
                .map(s -> buildSensor(station, s))
                .collect(Collectors.toList());
    }

    private Sensor buildSensor(final Station station, final SensorBO bo) {
        final String code = bo.getParameterCode();

        Sensor sensor = new Sensor();
        sensor.setExternalApiId(bo.getId());
        sensor.setMeasuredParameterCode(
                MeasurementType.getByCode(code).orElseThrow(() -> new NotKnownMeasurementTypeCodeException(code)));
        sensor.setMeasuredParameterDescription(bo.getParameterDescription());
        sensor.setStation(station);
        return sensor;
    }
}
