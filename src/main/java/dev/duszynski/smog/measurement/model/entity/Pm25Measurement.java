package dev.duszynski.smog.measurement.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PM25")
public class Pm25Measurement extends Measurement {
}
