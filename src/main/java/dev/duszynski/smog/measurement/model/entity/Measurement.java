package dev.duszynski.smog.measurement.model.entity;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.station.model.entity.Sensor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType = DiscriminatorType.STRING)
@Getter
@Setter
public abstract class Measurement {

    public static final String MEASURED_AT = "measuredAt";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double value;

    private Instant measuredAt;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private MeasurementType type;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sensor")
    private Sensor sensor;
}
