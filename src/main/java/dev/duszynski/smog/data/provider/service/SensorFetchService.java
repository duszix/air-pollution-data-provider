package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.data.provider.model.bo.SensorBO;
import dev.duszynski.smog.data.provider.model.dto.GiosSensorDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
class SensorFetchService {

    private static final String STATION_ID = "{stationId}";
    private static final String STATION_SENSORS_URL = "http://api.gios.gov.pl/pjp-api/rest/station/sensors/" + STATION_ID;

    private final RestTemplate restTemplate;

    public SensorFetchService() {
        this.restTemplate = new RestTemplate();
    }

    List<SensorBO> fetchSensorsForStation(final long idStation) {
        String stationSensorsUrl = STATION_SENSORS_URL.replace(STATION_ID, String.valueOf(idStation));
        final ResponseEntity<List<GiosSensorDTO>> response = restTemplate.exchange(stationSensorsUrl, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });

        if (response.getStatusCode() != HttpStatus.OK) {
            return Collections.emptyList();
        }

        List<GiosSensorDTO> body = response.getBody();

        if (body == null || body.isEmpty()) {
            return Collections.emptyList();
        }

        return body.stream().map(SensorBO::of).collect(Collectors.toList());
    }
}
