package dev.duszynski.smog.data.provider.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Station data returned by GIOS API
 */
@Getter
@Setter
public class GiosStationDTO {

    private long id;
    private String stationName;
    private float gegrLat;
    private float gegrLon;
    private String addressStreet;
    private GiosCityDTO city;
}
