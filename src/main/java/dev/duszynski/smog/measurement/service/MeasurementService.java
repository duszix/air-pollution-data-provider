package dev.duszynski.smog.measurement.service;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.config.AsyncConfiguration;
import dev.duszynski.smog.data.provider.model.bo.MeasurementBO;
import dev.duszynski.smog.data.provider.service.GiosSmogDataProvider;
import dev.duszynski.smog.measurement.dao.MeasurementRepository;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import dev.duszynski.smog.station.model.entity.Sensor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class MeasurementService {

    private final MeasurementRepository measurementRepository;
    private final GiosSmogDataProvider giosSmogDataProvider;

    public MeasurementService(MeasurementRepository measurementRepository, GiosSmogDataProvider giosSmogDataProvider) {
        this.measurementRepository = measurementRepository;
        this.giosSmogDataProvider = giosSmogDataProvider;
    }

    public Page<Measurement> fetchMeasurementsWithCriteria(final long idStation, final PageRequest pageRequest) {
        return measurementRepository.findAllBySensorStationId(idStation, pageRequest);
    }

    public Page<Measurement> fetchMeasurementsForTypeWithCriteria(final long idStation,
                                                                  final MeasurementType type,
                                                                  final PageRequest pageRequest) {
        return measurementRepository.findAllBySensorStationIdAndType(idStation, type, pageRequest);
    }

    @Async(AsyncConfiguration.SMOG_POOL_EXECUTOR)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void fetchNewestMeasurementForSensor(final Sensor sensor) {
        final Measurement lastMeasurement = getLastMeasurementForTypeAndSensor(sensor);

        giosSmogDataProvider.fetchMeasurementForSensor(sensor.getExternalApiId())
                .ifPresent(measurement -> {
                    if (isNewMeasurement(lastMeasurement, measurement)) {
                        buildAndPersistMeasurement(sensor, measurement);
                        log.info("Saved new measurement {} for sensor {}", measurement.getType().name(), sensor.getId());
                    }
                });
    }

    private Measurement getLastMeasurementForTypeAndSensor(final Sensor sensor) {
        return measurementRepository.findTopByTypeAndSensorIdOrderByMeasuredAtDesc(
                    sensor.getMeasuredParameterCode(),
                    sensor.getId())
                    .orElse(null);
    }

    private boolean isNewMeasurement(final Measurement lastMeasurement, final MeasurementBO measurement) {
        return lastMeasurement != null && !lastMeasurement.getMeasuredAt().equals(measurement.getMeasuredAt());
    }

    private void buildAndPersistMeasurement(final Sensor sensor, final MeasurementBO measurementBO) {
        final Measurement measurement = MeasurementFactory.createFor(measurementBO.getType());
        measurement.setMeasuredAt(measurementBO.getMeasuredAt());
        measurement.setValue(measurementBO.getValue());
        measurement.setSensor(sensor);
        measurementRepository.save(measurement);
    }
}
