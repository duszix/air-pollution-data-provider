package dev.duszynski.smog.data.provider.service;

import dev.duszynski.smog.data.provider.model.dto.GiosMeasurementDTO;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
class MeasurementFetcher {

    private static final String SENSOR_ID_PARAM_NAME = "{sensorId}";
    private static final String URL = "http://api.gios.gov.pl/pjp-api/rest/data/getData/" + SENSOR_ID_PARAM_NAME;

    private final RestTemplate restTemplate;

    public MeasurementFetcher() {
        this.restTemplate = new RestTemplate();
    }

    public Optional<GiosMeasurementDTO> fetchCurrentMeasurementsForSensor(final long idSensor) {
        final String stationMeasurementUrl = URL.replace(SENSOR_ID_PARAM_NAME, String.valueOf(idSensor));
        ResponseEntity<GiosMeasurementDTO> response = restTemplate.exchange(stationMeasurementUrl, HttpMethod.GET, null, GiosMeasurementDTO.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            return Optional.empty();
        }

        return Optional.ofNullable(response.getBody());
    }
}
