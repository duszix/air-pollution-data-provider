CREATE TABLE measurement
(
    id bigserial NOT NULL,
    value double precision NOT NULL,
    measured_at timestamp without time zone NOT NULL,
    type character varying(20) NOT NULL,
    id_sensor bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT measurement_sensor_id_fk FOREIGN KEY (id_sensor)
        REFERENCES sensor (id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);