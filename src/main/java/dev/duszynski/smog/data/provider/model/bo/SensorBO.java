package dev.duszynski.smog.data.provider.model.bo;

import dev.duszynski.smog.data.provider.model.dto.GiosSensorDTO;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorBO {

    private long id;
    private long idStation;
    private String parameterCode;
    private String parameterDescription;
    private long idParameter;

    public static SensorBO of(final GiosSensorDTO dto) {
        final SensorBO sensor = new SensorBO();
        final GiosSensorDTO.SensorParamDTO measuredParameter = dto.getParam();
        sensor.setId(dto.getId());
        sensor.setIdStation(dto.getStationId());
        sensor.setIdParameter(measuredParameter.getIdParam());
        sensor.setParameterDescription(getValueOrEmptyString(measuredParameter.getParaName()));
        sensor.setParameterCode(getValueOrEmptyString(measuredParameter.getParamCode()));
        return sensor;
    }

    private static String getValueOrEmptyString(final String value) {
        return value == null ? "" : value;
    }
}
