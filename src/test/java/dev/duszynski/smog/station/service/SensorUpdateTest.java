package dev.duszynski.smog.station.service;

import com.google.common.collect.Lists;
import dev.duszynski.smog.BaseIntegrationTest;
import dev.duszynski.smog.common.model.type.EntityStatus;
import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.data.provider.model.bo.SensorBO;
import dev.duszynski.smog.data.provider.service.GiosSmogDataProvider;
import dev.duszynski.smog.station.dao.SensorRepository;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Sensor;
import dev.duszynski.smog.station.model.entity.Station;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collection;

public class SensorUpdateTest extends BaseIntegrationTest {

    @Autowired
    private StationRepository stationRepository;

    @MockBean
    private GiosSmogDataProvider giosSmogDataProvider;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private SensorRepository sensorRepository;

    @Test
    public void shouldAddStationSensors() {
        // given
        int externalApiStationId = 5;

        final Station persistedStation = seedStation(externalApiStationId);
        long idStation = persistedStation.getId();

        Mockito.when(giosSmogDataProvider.fetchSensorsForStation(externalApiStationId)).thenReturn(
                Lists.newArrayList(
                        SensorBO.builder()
                                .parameterCode(MeasurementType.PM10.getGiosCode())
                                .parameterDescription("test")
                                .idStation(idStation)
                                .build())
        );

        // when
        sensorService.doFetchAndAddNewSensorsForStation(idStation);

        // then
        Collection<Sensor> sensors = sensorRepository.findAllByStationId(idStation);

        Assertions.assertEquals(1, sensors.size());
    }

    @Test
    public void shouldNotAddSameStationSensor() {
        // given
        int externalApiStationId = 5;

        final Station persistedStation = seedStation(externalApiStationId);
        long idStation = persistedStation.getId();

        int externalSensorApiId = 4;
        final Sensor sensor = new Sensor();
        sensor.setStation(persistedStation);
        sensor.setExternalApiId(externalSensorApiId);
        sensor.setMeasuredParameterDescription("test");
        sensor.setMeasuredParameterCode(MeasurementType.PM10);
        sensorRepository.save(sensor);
        persistedStation.setSensors(Lists.newArrayList(sensor));

        Mockito.when(giosSmogDataProvider.fetchSensorsForStation(externalApiStationId)).thenReturn(
                Lists.newArrayList(SensorBO.builder().id(externalSensorApiId).parameterCode("5").idStation(idStation).build())
        );

        // when
        sensorService.doFetchAndAddNewSensorsForStation(idStation);

        // then
        Collection<Sensor> sensors = sensorRepository.findAllByStationId(idStation);

        Assertions.assertEquals(1, sensors.size());
    }

    private Station seedStation(int externalApiStationId) {
        final Station station = new Station();
        station.setName("test-station");
        station.setExternalId(externalApiStationId);
        station.setStatus(EntityStatus.ACTIVE);
        return stationRepository.save(station);
    }
}
