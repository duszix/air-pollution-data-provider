package dev.duszynski.smog.data.provider.model.bo;

import dev.duszynski.smog.common.model.GeoPoint;
import dev.duszynski.smog.data.provider.model.dto.GiosCityDTO;
import dev.duszynski.smog.data.provider.model.dto.GiosStationDTO;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class StationBO {

    private static final String EMPTY_STRING = "";

    private long externalSystemId;
    private String name;
    private GeoPoint geoPoint;
    private String city;
    private String street;

    public static StationBO of(GiosStationDTO dto) {
        final GiosCityDTO city = dto.getCity();
        return StationBO.builder()
                .externalSystemId(dto.getId())
                .name(dto.getStationName())
                .geoPoint(GeoPoint.of(dto.getGegrLat(), dto.getGegrLon()))
                .city(Objects.nonNull(city) ? city.getName() : EMPTY_STRING)
                .street(dto.getAddressStreet())
                .build();
    }
}
