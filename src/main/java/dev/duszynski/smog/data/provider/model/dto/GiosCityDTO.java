package dev.duszynski.smog.data.provider.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiosCityDTO {

    private long id;
    private String name;
}
