package dev.duszynski.smog.station.model.entity;

import dev.duszynski.smog.common.model.type.EntityStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Station {

    public static final String NAME = "name";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long externalId;

    @NotNull
    private String name;

    private String city;

    private String street;

    private Double latitude;

    private Double longitude;

    @Enumerated(EnumType.STRING)
    private EntityStatus status;

    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<Sensor> sensors = new ArrayList<>();
}
