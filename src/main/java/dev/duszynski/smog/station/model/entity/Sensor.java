package dev.duszynski.smog.station.model.entity;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_station")
    private Station station;

    @NotNull
    private String measuredParameterDescription;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MeasurementType measuredParameterCode;

    @Column(name = "external_id")
    private long externalApiId;

    @OneToMany(mappedBy = "sensor", fetch = FetchType.LAZY)
    private List<Measurement> measurements = new ArrayList<>();
}
