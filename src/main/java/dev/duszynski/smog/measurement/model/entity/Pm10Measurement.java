package dev.duszynski.smog.measurement.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PM10")
public class Pm10Measurement extends Measurement {
}
