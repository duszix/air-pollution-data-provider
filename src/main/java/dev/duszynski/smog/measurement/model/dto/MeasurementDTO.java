package dev.duszynski.smog.measurement.model.dto;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class MeasurementDTO {

    private Instant measuredAt;
    private double value;
    private MeasurementType type;

    public static MeasurementDTO of(final Measurement measurement) {
        return MeasurementDTO
                .builder()
                .measuredAt(measurement.getMeasuredAt())
                .value(measurement.getValue())
                .type(measurement.getType())
                .build();
    }
}
