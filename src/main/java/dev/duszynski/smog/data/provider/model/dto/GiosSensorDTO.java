package dev.duszynski.smog.data.provider.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiosSensorDTO {

    private long id;
    private long stationId;
    private SensorParamDTO param;

    @Getter
    @Setter
    public class SensorParamDTO {
        private String paraName;
        private String paramFormula;
        private String paramCode;
        private long idParam;
    }
}
