package dev.duszynski.smog.measurement.api;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.model.dto.MeasurementDTO;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import dev.duszynski.smog.measurement.service.MeasurementService;
import dev.duszynski.smog.station.model.entity.Station;
import dev.duszynski.smog.station.service.StationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@Transactional
@RequestMapping(path = "/api/stations")
public class MeasurementController {

    private final StationService stationService;
    private final MeasurementService measurementService;

    public MeasurementController(StationService stationService, MeasurementService measurementService) {
        this.stationService = stationService;
        this.measurementService = measurementService;
    }

    @GetMapping(path = "/{idStation}/measurements")
    public ResponseEntity<Page<MeasurementDTO>> getMeasurementsForStation(
            @PathVariable long idStation,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Integer pageNumber) {

        final Station station = stationService.getByIdOrThrow(idStation);
        return ResponseEntity.ok(
                measurementService
                        .fetchMeasurementsWithCriteria(idStation, buildPageDataRequest(pageSize, pageNumber))
                        .map(MeasurementDTO::of));
    }

    @GetMapping(path = "/{idStation}/measurements/type/{type}")
    public ResponseEntity<Page<MeasurementDTO>> getMeasurementsForStation(
            @PathVariable long idStation,
            @PathVariable MeasurementType type,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Integer pageNumber) {

        final Station station = stationService.getByIdOrThrow(idStation);
        return ResponseEntity.ok(
                measurementService
                        .fetchMeasurementsForTypeWithCriteria(idStation, type, buildPageDataRequest(pageSize, pageNumber))
                        .map(MeasurementDTO::of));
    }

    private PageRequest buildPageDataRequest(final Integer pageSize, final Integer pageNumber) {
        return PageRequest.of(
                pageNumber == null ? 0 : pageNumber,
                pageSize == null ? Integer.MAX_VALUE : pageSize,
                Sort.by(Sort.Direction.DESC, Measurement.MEASURED_AT));
    }
}
