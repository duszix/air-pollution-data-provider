package dev.duszynski.smog.common.model.type;

import java.util.Arrays;
import java.util.Optional;

public enum MeasurementType {

    PM25("PM2.5"),
    PM10("PM10"),
    NO2("NO2"),
    CO("CO"),
    O3("O3"),
    SO2("SO2"),
    C6H6("C6H6");

    private final String giosCode;

    MeasurementType(String giosCode) {
        this.giosCode = giosCode;
    }

    public String getGiosCode() {
        return giosCode;
    }

    public static Optional<MeasurementType> getByCode(final String code) {
        return Arrays.stream(values()).filter(v -> v.getGiosCode().equals(code)).findFirst();
    }
}
