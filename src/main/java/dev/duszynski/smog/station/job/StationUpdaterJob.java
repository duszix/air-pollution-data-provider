package dev.duszynski.smog.station.job;

import dev.duszynski.smog.common.model.GeoPoint;
import dev.duszynski.smog.data.provider.model.bo.StationBO;
import dev.duszynski.smog.data.provider.service.GiosSmogDataProvider;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Station;
import dev.duszynski.smog.common.model.type.EntityStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class StationUpdaterJob {

    private final StationRepository stationRepository;
    private final GiosSmogDataProvider giosSmogDataProvider;

    public StationUpdaterJob(StationRepository stationRepository, GiosSmogDataProvider giosSmogDataProvider) {
        this.stationRepository = stationRepository;
        this.giosSmogDataProvider = giosSmogDataProvider;
    }

    @Transactional
    @Scheduled(cron = "0 0 */10 * * *")
    public void checkForStationModifications() {
        List<Station> persistedStations = stationRepository.findAll();

        List<StationBO> currentStations = giosSmogDataProvider.fetchAllStations();
        createNewStations(persistedStations, currentStations);

        markNotPresentStationsAsInactive(persistedStations, currentStations);
    }

    private void createNewStations(final List<Station> persistedStations, final List<StationBO> currentStations) {
        findNewStations(currentStations, persistedStations).forEach(s -> {
            Station station = buildStationFrom(s);

            final GeoPoint stationGeoPoint = s.getGeoPoint();
            if (stationGeoPoint != null) {
                station.setLatitude(stationGeoPoint.getLatitude());
                station.setLongitude(stationGeoPoint.getLongitude());
            }

            stationRepository.save(station);
        });
    }

    private Station buildStationFrom(final StationBO s) {
        final Station station = new Station();
        station.setExternalId(s.getExternalSystemId());
        station.setName(s.getName());
        station.setCity(s.getCity());
        station.setStreet(s.getStreet());
        station.setStatus(EntityStatus.ACTIVE);
        return station;
    }

    private List<StationBO> findNewStations(final List<StationBO> currentStations,
                                            final List<Station> persistedStations) {
        final List<Long> alreadyKnownStationExternalIds =
                persistedStations.stream().map(Station::getExternalId).collect(Collectors.toList());

        List<StationBO> newStations = currentStations.stream()
                .filter(station -> !alreadyKnownStationExternalIds.contains(station.getExternalSystemId()))
                .collect(Collectors.toList());

        log.info("Found {} new stations {}", newStations.size(), newStations);

        return newStations;
    }

    private void markNotPresentStationsAsInactive(final List<Station> persistedStations,
                                                  final List<StationBO> currentStations) {
        final Set<Long> fetchedStationExternalIds = currentStations.stream()
                .map(StationBO::getExternalSystemId)
                .collect(Collectors.toSet());

        persistedStations.stream().filter(s -> !fetchedStationExternalIds.contains(s.getExternalId()))
                .forEach(station -> {
                    station.setStatus(EntityStatus.INACTIVE);
                    stationRepository.save(station);
                });
    }
}
