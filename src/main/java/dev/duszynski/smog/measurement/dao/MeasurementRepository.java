package dev.duszynski.smog.measurement.dao;

import dev.duszynski.smog.common.model.type.MeasurementType;
import dev.duszynski.smog.measurement.model.entity.Measurement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MeasurementRepository extends JpaRepository<Measurement, Long> {

    Optional<Measurement> findTopByTypeAndSensorIdOrderByMeasuredAtDesc(final MeasurementType type,
                                                                        final Long idSensor);

    Page<Measurement> findAllBySensorStationId(final long idStation, final Pageable pageable);

    Page<Measurement> findAllBySensorStationIdAndType(final long idStation,
                                                      final MeasurementType type,
                                                      final Pageable pageable);

}
