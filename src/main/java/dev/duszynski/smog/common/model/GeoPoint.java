package dev.duszynski.smog.common.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeoPoint {

    private double latitude;
    private double longitude;

    private GeoPoint() {}

    public static GeoPoint of(double latitude, double longitude) {
        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(latitude);
        geoPoint.setLongitude(longitude);
        return geoPoint;
    }
}
