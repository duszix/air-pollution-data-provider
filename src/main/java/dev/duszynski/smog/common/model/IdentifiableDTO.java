package dev.duszynski.smog.common.model;

import lombok.Getter;

@Getter
public abstract class IdentifiableDTO<E extends Number> {

    protected E id;
}
