# Air pollution data provider

Very basic demo project used to fetch data from GIOS Poland services about air pollution.
Quickly written, available and open to many enhancements. 
Data stored locally for further analyse purposes and statistics. 

Application fetches measurement stations, their sensors and sensors' reads.
New stations and sensors checked regularly. PM2.5 and PM10 reads fetched every 30 minutes. 

Exposes REST API with data able to be fetched by client applications.

More info about GIOS API available at https://powietrze.gios.gov.pl/pjp/content/api. 
GIOS API description only in polish language.

## Technologies
* Java 11
* Spring Boot 2
* PostgreSQL
* Hibernate
* Flyway
* Maven
* Lombok

### Prerequisites
RDBMS server installed. Used PostgreSQL but can use other.

### Installation
Properties file located in `resources` folder. 
Copy `application-example.yml` file, name as `application.yml` (if want to use one profile only) and put your own properties.

Maven goal `spring-boot:run` can be used to run service.
JAR file can be built with maven and executed with command `java -jar path-to-jar-file`

### Tests
JUnit used for testing. Currently only some integration tests are written.

### API
No API documentation. Add swagger later.