package dev.duszynski.smog.data.provider.service;

import com.google.common.collect.Lists;
import dev.duszynski.smog.BaseIntegrationTest;
import dev.duszynski.smog.common.model.type.EntityStatus;
import dev.duszynski.smog.data.provider.model.bo.StationBO;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.job.StationUpdaterJob;
import dev.duszynski.smog.station.model.entity.Station;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;

public class StationUpdaterTest extends BaseIntegrationTest {

    @Autowired
    private StationRepository stationRepository;

    @MockBean
    private StationFetchService stationFetchService;

    @Autowired
    private StationUpdaterJob stationUpdaterJob;

    @Test
    public void shouldAddNewStation() {
        // given
        Station station = new Station();
        station.setName("test1");
        station.setExternalId(1);
        station.setStatus(EntityStatus.ACTIVE);

        stationRepository.save(station);

        Mockito.when(stationFetchService.fetchAllAvailableStations()).thenReturn(
                Lists.newArrayList(StationBO.builder().externalSystemId(2).name("test2").build())
        );

        // when
        stationUpdaterJob.checkForStationModifications();

        // then
        List<Station> stations = stationRepository.findAll();

        Assertions.assertEquals(2, stations.size());
    }

    @Test
    public void shouldNotAddSameStationBecauseOfSameExternalId() {
        // given
        Station station = new Station();
        station.setName("test1");
        station.setExternalId(1);
        station.setStatus(EntityStatus.ACTIVE);

        stationRepository.save(station);

        Mockito.when(stationFetchService.fetchAllAvailableStations()).thenReturn(
                Lists.newArrayList(StationBO.builder().externalSystemId(1).build())
        );

        // when
        stationUpdaterJob.checkForStationModifications();

        // then
        List<Station> stations = stationRepository.findAll();

        Assertions.assertEquals(1, stations.size());
    }

    @Test
    public void shouldMarkNotFetchedStationAsInactive() {
        // given
        Station station = new Station();
        station.setName("test1");
        station.setExternalId(1);
        station.setStatus(EntityStatus.ACTIVE);

        stationRepository.save(station);

        Mockito.when(stationFetchService.fetchAllAvailableStations()).thenReturn(Collections.emptyList());

        // when
        stationUpdaterJob.checkForStationModifications();

        // then
        List<Station> stations = stationRepository.findAll();

        Assertions.assertEquals(1, stations.size());
        Assertions.assertEquals(EntityStatus.INACTIVE, stations.get(0).getStatus());
    }
}
