package dev.duszynski.smog.data.provider.model.bo;

import dev.duszynski.smog.common.model.type.MeasurementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class MeasurementBO {

    private Instant measuredAt;
    private double value;
    private MeasurementType type;

    public static MeasurementBO of(final MeasurementType type, final double value, final Instant measuredAt) {
        return MeasurementBO.builder().type(type).measuredAt(measuredAt).value(value).build();
    }
}
