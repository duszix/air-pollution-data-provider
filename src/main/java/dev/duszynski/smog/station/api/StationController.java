package dev.duszynski.smog.station.api;

import dev.duszynski.smog.station.model.dto.StationDTO;
import dev.duszynski.smog.station.service.StationService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping(path = "/api/stations")
public class StationController {

    private final StationService stationService;

    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    @GetMapping
    public ResponseEntity<Page<StationDTO>> getStations(
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Integer pageNumber) {

        return ResponseEntity.ok(
                stationService.fetchStationsWithCriteria(pageSize, pageNumber).map(StationDTO::of));
    }
}
