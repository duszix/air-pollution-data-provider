package dev.duszynski.smog.data.provider.service;

import com.google.common.collect.Lists;
import dev.duszynski.smog.data.provider.model.dto.GiosMeasurementDTO;
import dev.duszynski.smog.data.provider.model.dto.GiosMeasurementValueDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

public class LastMeasurementValueTest {

    MeasurementFetchService measurementFetchService = new MeasurementFetchService(null);

    @Test
    public void shouldReturnLastMeasurement() {
        // given
        GiosMeasurementValueDTO value1 = new GiosMeasurementValueDTO();
        value1.setDate("2017-03-28 11:00:00");
        value1.setValue(30.3018);

        GiosMeasurementValueDTO value2 = new GiosMeasurementValueDTO();
        value2.setDate("2017-03-28 12:00:00");
        value2.setValue(27.5946);

        GiosMeasurementDTO dto = new GiosMeasurementDTO();
        dto.setValues(Lists.newArrayList(value1, value2));

        // when
        Optional<GiosMeasurementValueDTO> lastMeasurement = measurementFetchService.fetchLastWithValue(dto);

        // then
        Assertions.assertTrue(lastMeasurement.isPresent());
        Assertions.assertEquals(value2, lastMeasurement.get());
    }

    @Test
    public void shouldReturnPreviousMeasurement() {
        // given
        GiosMeasurementValueDTO value1 = new GiosMeasurementValueDTO();
        value1.setDate("2017-03-28 11:00:00");
        value1.setValue(27.5946);

        GiosMeasurementValueDTO value2 = new GiosMeasurementValueDTO();
        value2.setDate("2017-03-28 12:00:00");
        value2.setValue(0);

        GiosMeasurementDTO dto = new GiosMeasurementDTO();
        dto.setValues(Lists.newArrayList(value1, value2));

        // when
        Optional<GiosMeasurementValueDTO> lastMeasurement = measurementFetchService.fetchLastWithValue(dto);

        // then
        Assertions.assertTrue(lastMeasurement.isPresent());
        Assertions.assertEquals(value1, lastMeasurement.get());
    }

    @Test
    public void shouldNotFetchAnyMeasurement() {
        // given
        GiosMeasurementValueDTO value1 = new GiosMeasurementValueDTO();
        value1.setDate("2017-03-28 11:00:00");
        value1.setValue(0);

        GiosMeasurementValueDTO value2 = new GiosMeasurementValueDTO();
        value2.setDate("2017-03-28 12:00:00");
        value2.setValue(0);

        GiosMeasurementDTO dto = new GiosMeasurementDTO();
        dto.setValues(Lists.newArrayList(value1, value2));

        // when
        Optional<GiosMeasurementValueDTO> lastMeasurement = measurementFetchService.fetchLastWithValue(dto);

        // then
        Assertions.assertTrue(lastMeasurement.isEmpty());
    }
}
