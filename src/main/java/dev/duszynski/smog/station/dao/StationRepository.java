package dev.duszynski.smog.station.dao;

import dev.duszynski.smog.common.model.type.EntityStatus;
import dev.duszynski.smog.station.model.entity.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface StationRepository extends JpaRepository<Station, Long> {

    Collection<Station> findAllByStatus(final EntityStatus status);
}
