package dev.duszynski.smog.station.job;

import dev.duszynski.smog.common.model.type.EntityStatus;
import dev.duszynski.smog.station.dao.StationRepository;
import dev.duszynski.smog.station.model.entity.Station;
import dev.duszynski.smog.station.service.SensorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class SensorUpdaterJob {

    private final StationRepository stationRepository;
    private final SensorService sensorService;

    public SensorUpdaterJob(final StationRepository stationRepository, final SensorService sensorService) {
        this.stationRepository = stationRepository;
        this.sensorService = sensorService;
    }

    @Transactional
    @Scheduled(cron = "0 0 0/20 * * *")
    public void checkForNewSensors() {
        stationRepository.findAllByStatus(EntityStatus.ACTIVE)
                .stream()
                .map(Station::getId)
                .forEach(sensorService::fetchAndAddNewSensorsForStation);
    }

}
