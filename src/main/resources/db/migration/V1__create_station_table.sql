CREATE TABLE station
(
    id bigserial NOT NULL,
    name character varying(255) NOT NULL,
    city character varying(255) NULL,
    street character varying(255) NULL,
    latitude double precision,
    longitude double precision,
    external_id bigint  NOT NULL,
    PRIMARY KEY (id)
);