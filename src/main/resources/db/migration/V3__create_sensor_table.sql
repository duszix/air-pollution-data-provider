CREATE TABLE sensor
(
    id bigserial NOT NULL,
    id_station bigint NOT NULL,
    measured_parameter_description character varying(255) NOT NULL,
    measured_parameter_code character varying(20) NOT NULL,
    external_id bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT sensor_station_id_fk FOREIGN KEY (id_station)
        REFERENCES station (id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);