package dev.duszynski.smog.common.model.type;

public enum EntityStatus {

    ACTIVE, INACTIVE, DELETED
}
