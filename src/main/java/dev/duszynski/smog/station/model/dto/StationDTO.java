package dev.duszynski.smog.station.model.dto;

import dev.duszynski.smog.common.model.IdentifiableDTO;
import dev.duszynski.smog.station.model.entity.Station;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StationDTO extends IdentifiableDTO<Long> {

    private String name;
    private Double latitude;
    private Double longitude;
    private String city;
    private String street;

    public static StationDTO of(final Station station) {
        StationDTO dto = new StationDTO();
        dto.id = station.getId();
        dto.name = station.getName();
        dto.latitude = station.getLatitude();
        dto.longitude = station.getLongitude();
        dto.city = station.getCity();
        dto.street = station.getStreet();
        return dto;
    }
}
